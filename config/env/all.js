'use strict';

module.exports = {
  app: {
    title: 'Vendimia',
    description: 'Ventas',
    keywords: 'ventas'
  },
  port: process.env.PORT || 3000,
  templateEngine: 'swig',
  // The secret should be set to a non-guessable string that
  // is used to compute a session hash
  sessionSecret: 'MEAN',
  // The name of the MongoDB collection to store sessions in
  sessionCollection: 'sessions',
  // The session cookie settings
  sessionCookie: {
    path: '/',
    httpOnly: true,
    // If secure is set to true then it will cause the cookie to be set
    // only when SSL-enabled (HTTPS) is used, and otherwise it won't
    // set a cookie. 'true' is recommended yet it requires the above
    // mentioned pre-requisite.
    secure: false,
    // Only set the maxAge to null if the cookie shouldn't be expired
    // at all. The cookie will expunge when the browser is closed.
    maxAge: null,
    // To set the cookie in a specific domain uncomment the following
    // setting:
    // domain: 'yourdomain.com'
  },
  // The session cookie name
  sessionName: 'connect.sid',
  log: {
    // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
    format: 'combined',
    // Stream defaults to process.stdout
    // Uncomment to enable logging to a log on the file system
    options: {
      stream: 'access.log'
    }
  },
  assets: {
    lib: {
      css: [
        'public/lib/materialize/dist/css/materialize.min.css',
        'public/lib/material-design-icons/iconfont/material-icons.css',
        'public/lib/perfect-scrollbar/min/perfect-scrollbar.min.css',
        'public/lib/angular-loading-bar/build/loading-bar.min.css',
				'public/lib/angucomplete-alt/angucomplete-alt.css'
      ],
      js: [
        'public/lib/jquery/dist/jquery.min.js',
        'public/lib/angular/angular.js',
        'public/lib/angular-resource/angular-resource.js',
        // 'public/lib/angular-animate/angular-animate.js',
        'public/lib/angular-ui-router/release/angular-ui-router.js',
        'public/lib/angular-ui-utils/ui-utils.min.js',

        'public/lib/angular-materialize/src/angular-materialize.js',
        'public/lib/materialize/dist/js/materialize.min.js',

        'public/lib/perfect-scrollbar/min/perfect-scrollbar.min.js',
        //perfect-scrollbar/min/perfect-scrollbar.with-mousewheel.min.js NOT FOUND
        'public/lib/angular-perfect-scrollbar/src/angular-perfect-scrollbar.js',

        'public/lib/underscore/underscore-min.js',
        'public/lib/angular-local-storage/dist/angular-local-storage.js',

        'public/lib/moment/moment.js',
        'public/lib/moment/locale/es.js',
        'public/lib/angular-moment/angular-moment.min.js',
        'public/lib/angular-loading-bar/build/loading-bar.min.js',
				'public/lib/angucomplete-alt/dist/angucomplete-alt.min.js'
      ]
    },
    css: [
      'public/modules/**/css/*.css'
    ],
    js: [
      'public/config.js',
      'public/application.js',
      'public/modules/*/*.js',
      'public/modules/*/*[!tests]*/*.js'
    ],
    tests: [
      'public/lib/angular-mocks/angular-mocks.js',
      'public/modules/*/tests/*.js'
    ]
  }
};
