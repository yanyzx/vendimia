'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function() {
  // Init module configuration options
  var applicationModuleName = 'vendimia';

  var applicationModuleVendorDependencies = [
    'ngResource',
    'ui.router',
    'ui.utils',
    'ui.materialize',
    'perfect_scrollbar',
    'LocalStorageModule',
    'angularMoment',
    'angular-loading-bar',
		'angucomplete-alt'
  ];

  // Add a new vertical module
  var registerModule = function(moduleName, dependencies) {
    // Create angular module
    angular.module(moduleName, dependencies || []);

    // Add the module to the AngularJS configuration file
    angular.module(applicationModuleName).requires.push(moduleName);
  };

  return {
    applicationModuleName: applicationModuleName,
    applicationModuleVendorDependencies: applicationModuleVendorDependencies,
    registerModule: registerModule
  };
})();

'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName,
               ApplicationConfiguration.applicationModuleVendorDependencies)
               .constant('API_URL', API_URL);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider',
  function($locationProvider) {
    $locationProvider.hashPrefix('!');
  }
]);

//Then define the init function for starting up the application
angular.element(document).ready(function() {
  //Fixing facebook bug with redirect
  if (window.location.hash === '#_=_') window.location.hash = '#!';



  //Then init the app
  angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});

'use strict';

// Use Application configuration module to register a new module
ApplicationConfiguration.registerModule('articles');

'use strict';

// Use Application configuration module to register a new module
ApplicationConfiguration.registerModule('clients');

'use strict';

// Use Application configuration module to register a new module
ApplicationConfiguration.registerModule('configurations');

'use strict';

// Use Application configuration module to register a new module
ApplicationConfiguration.registerModule('core');

'use strict';

// Use Application configuration module to register a new module
ApplicationConfiguration.registerModule('home');
'use strict';

// Use Application configuration module to register a new module
ApplicationConfiguration.registerModule('sales');

(function() {
'use strict';

  function ArticlesConfig($stateProvider) {

    $stateProvider
    .state('articles', {
      url: '/articulos',
      parent: 'home',
      templateUrl: 'modules/articles/views/articles.client.view.html',
      controller: 'ArticlesCtrl'
    });

  }

  angular.module('articles').config([
    '$stateProvider',
    ArticlesConfig
  ]);
})();

(function() {
  'use strict';

  function ArticlesCtrl($scope, AppConfig, ArticlesAPI, $q) {
    AppConfig.setTitle('Artículos');
    var _this = this;
		$scope.isContentLoaded = false;
    $scope.articles = [];

		this.fetchArticles = function() {
			ArticlesAPI.query().$promise
			.then(function(articles) {
				$scope.articles = articles;
			})
			.catch(function(err) {
				Materialize.toast('No se pudo obtener articulos, intente nuevamente', 4000);
			})
			.finally(function() {
				$scope.isContentLoaded = true;
			});
		}

    this.fetchArticles();
  }

  angular.module('sales').controller('ArticlesCtrl', [
    '$scope',
    'AppConfig',
    'ArticlesAPI',
    '$q',
    ArticlesCtrl
  ]);
})();

(function() {
'use strict';

  function ClientsConfig($stateProvider) {

    $stateProvider
    .state('clients', {
      url: '/clientes',
      parent: 'home',
      templateUrl: 'modules/clients/views/clients.client.view.html',
      controller: 'ClientsCtrl'
    });

  }

  angular.module('sales').config([
    '$stateProvider',
    ClientsConfig
  ]);
})();

(function() {
  'use strict';

  function ClientsCtrl($scope, AppConfig, ClientsAPI, $q) {
    AppConfig.setTitle('Clientes');
    var _this = this;
		$scope.isContentLoaded = false;
    $scope.clients = [];

		this.fetchClients = function() {
			ClientsAPI.query().$promise
			.then(function(clients) {
				$scope.clients = clients;
			})
			.catch(function(err) {
				Materialize.toast('No se pudo obtener clientes, intente nuevamente', 4000);
			})
			.finally(function() {
				$scope.isContentLoaded = true;
			});
		}

    this.fetchClients();
  }

  angular.module('clients').controller('ClientsCtrl', [
    '$scope',
    'AppConfig',
    'ClientsAPI',
    '$q',
    ClientsCtrl
  ]);
})();

(function() {
'use strict';

  function ConfigurationsConfig($stateProvider) {

    $stateProvider
    .state('configurations', {
      url: '/configuracion',
      parent: 'home',
      templateUrl: 'modules/configurations/views/configurations.client.view.html',
      controller: 'ConfigurationsCtrl'
    });

  }

  angular.module('configurations').config([
    '$stateProvider',
    ConfigurationsConfig
  ]);
})();

(function() {
  'use strict';

  function ConfigurationsCtrl($scope, AppConfig, ConfigurationsAPI, $q) {
    AppConfig.setTitle('Configuración');
    var _this = this;
		$scope.isContentLoaded = false;
    $scope.configurations = [];

		this.fetchConfigurations = function() {
			ConfigurationsAPI.findOne().$promise
			.then(function(configurations) {
				$scope.configurations[0] = configurations;
			})
			.catch(function(err) {
				Materialize.toast('No se pudo obtener configuraciones, intente nuevamente', 4000);
			})
			.finally(function() {
				$scope.isContentLoaded = true;
			});
		}

    this.fetchConfigurations();
  }

  angular.module('configurations').controller('ConfigurationsCtrl', [
    '$scope',
    'AppConfig',
    'ConfigurationsAPI',
    '$q',
    ConfigurationsCtrl
  ]);
})();

'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
  function($stateProvider, $urlRouterProvider) {
    // Redirect to home view when route not found
    $urlRouterProvider.otherwise('/inicio');

    // Home state routing
    $stateProvider
    .state('home', {
      // url: '',
      templateUrl: 'modules/core/views/home.client.view.html'
    });
    
  }
]);
(function() {
  'use strict';

  function CoreRun($rootScope, $state, amMoment) {

    amMoment.changeLocale('es');

    var scrollTop = function() {
      angular.element('html,body').scrollTop(0);
    };
    
    var onStateChangeSuccess = function(event, toState, toParams, fromState,
                                      fromParams, error) {

      scrollTop();

     // event.preventDefault();
     //  console.log(toState)
     //  if(toState.name == 'home')
     //  	$state.go('main');
      
    };

    var listenForStateChangeSuccess = function() {
      $rootScope.$on('$stateChangeSuccess', onStateChangeSuccess);  
    };
    
    listenForStateChangeSuccess();
  }

  angular.module('core').run([
    '$rootScope',
    '$state',
    'amMoment',
    CoreRun
  ]);
})();
(function() {
  'use strict';

  function LoadingBarConfig(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
    cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
  }

  angular.module('core').config([
    'cfpLoadingBarProvider',
    LoadingBarConfig
  ]);
})();

(function(){
'use strict';
  function loader() {

    function link(scope, element, attrs) {
      scope.color = scope.color || 'default';
      scope.ngStyle = {
        width: scope.size + 'px',
        height: scope.size + 'px'
      };
    }

    return {
      restrict: 'E',
      templateUrl:'modules/core/views/loader.client.view.html',
      link: link,
      scope: {
        size: '=',
        color: '=?',
        customClass: '='
      }
    };
  }

  angular.module('core').directive('loader', [
    loader
  ]);
})();
(function() {
  'use strict';

  function TopbarDirective() {
    function link(scope, element, attrs) {
      var setDefaultValues = function() {
        scope.padding = scope.padding || 50;
        scope.iconSize = scope.iconSize || 140;
        scope.titleSize = scope.titleSize || 14;
        scope.messageSize = scope.messageSize || 14;
        scope.iconClass = scope.iconClass || '';
      };

      setDefaultValues();
      
      scope.containerStyle = {
        padding: scope.padding + 'px 0'
      };

      scope.iconStyle = {
        'font-size': scope.iconSize + 'px !important'
      };
      scope.iconStyle = 'font-size: ' + scope.iconStyle['font-size'];

      scope.titleStyle = {
        'font-size': scope.titleSize + 'px'
      };

      scope.messageStyle = {
        'font-size': scope.messageSize + 'px'
      };

      scope.ngStyle = {
        'font-size': scope.fontSize + 'px',
        height: scope.height + 'px'
      };

    }

    return {
      templateUrl: 'modules/core/views/message-with-icon.client.view.html',
      scope: {
        padding: '@',
        iconClass: '@',
        iconSize: '@',
        iconText: '@', 
        title: '@',
        titleSize: '@',
        message: '@',
        messageSize: '@'
      },
      link:  link,
      restrict: 'E'
    };
  }

  angular.module('core').directive('messageWithIcon', [
    TopbarDirective
  ]);
})();
(function(){
'use strict';
  function noRecordMessage() {
    function link (scope, element, attrs) {
      attrs.message = attrs.message || 'No hay registros para mostrar';
    }

    return {
      templateUrl:'modules/core/views/no-record-message.client.view.html',
      restrict: 'E',
      link: link,
      scope: {
        message: '@'
      }
    };
  }

  angular.module('core').directive('noRecordMessage', [
    noRecordMessage
  ]);
})();

(function() {
  'use strict';

  function PreLoaderDirective($rootScope) {
    return {
      templateUrl: 'modules/core/views/pre-loader.client.view.html',
      restrict: 'E'
    };
  }

  angular.module('core').directive('preLoader', [
    '$rootScope',
    PreLoaderDirective
  ]);
})();
(function () {
  'use strict';

  var inputTypes = {
    textNumber: '[^A-z0-9 _-ñÑáéíóúÁÉÍÓÚ]',
    text: '[^A-z _-ñÑáéíóúÁÉÍÓÚ]',
    number: '[^0-9]'
  };

  var Restrict = function ($parse) {
    var link = function ($scope, $element, $attrs) {
      var exp = inputTypes[$attrs.restrict] || $attrs.restrict;
      $scope.$watch($attrs.ngModel, function (value) {
        if (!value) {
          return;
        }
        if (typeof value === 'number') {
          value = value + '';
        }
        $parse($attrs.ngModel).assign($scope, value.replace(new RegExp(exp, 'g'), ''));
      });
    };

    return {
      restrict: 'A',
      require: 'ngModel',
      link: link
    };
  };

  angular.module('core').directive('restrict', [
    '$parse',
    Restrict
  ]);
})();

(function() {
  'use strict';

  function TopbarDirective($rootScope, $timeout, $interval, $state, $stateParams) {
    function link(scope, element, attrs) {

      scope.title = $state.current.appTitle;

      // Hide Header on on scroll down

      var lastScrollTop = 0;
      var move = 0;
      var top = 0;
      var topbarHeight = angular.element('topbar nav').outerHeight() * -1;

      function onScroll(element) {
        var topDistance = $(element).scrollTop();

        var move =  topDistance - lastScrollTop;
        if (topDistance > lastScrollTop) {
          // Scroll Down
          top = top - move;
          if(top < topbarHeight)
            top = topbarHeight;

          angular.element('topbar nav').css('top', top+'px');
        } else {
          // Scroll Up
          top = top - move;
          if(top > 0)
            top = 0;

          angular.element('topbar nav').css('top', top+'px');
        }

        lastScrollTop = topDistance;
      };

      angular.element(window).scroll(function(event){
        onScroll(window);
      });

    }

    return {
      templateUrl: 'modules/core/views/topbar.client.view.html',
      restrict: 'E',
      link: link
    };
  }

  angular.module('core').directive('topbar', [
    '$rootScope',
    '$timeout',
    '$interval',
    '$state',
    '$stateParams',
    TopbarDirective
  ]);
})();

(function() {
  'use strict';

  function AppConfig($rootScope) {

    return {
      setTitle: function (title, isFull) {
        var DEFAULT_APP_TITLE = 'Vendimia - Venta de artículos en plazos';

        if (title && isFull) {
          $rootScope.title = title;
        }
        else if (title) {
          $rootScope.title = title + ' - Vendimia';
        }
        else {
          $rootScope.title = DEFAULT_APP_TITLE;
        }

      }
    };

  }

  angular.module('core').factory('AppConfig', [
    '$rootScope',
    AppConfig
  ]);
})();

(function() {
  'use strict';

  function ArticlesAPI($resource, API_URL) {
    return $resource(API_URL + '/articles/:id', {}, {
      update: { method: 'PUT' },
      count: {
        method: 'GET',
        url: API_URL + '/articles/count',
        isArray: false,
        responseType: 'json'
      }
    });
  }

  angular.module('core').factory('ArticlesAPI', [
    '$resource',
    'API_URL',
    ArticlesAPI
  ]);
})();

(function() {
  'use strict';

  function ClientsAPI($resource, API_URL) {
    return $resource(API_URL + '/clients/:id', {}, {
      update: { method: 'PUT' },
      count: {
        method: 'GET',
        url: API_URL + '/clients/count',
        isArray: false,
        responseType: 'json'
      }
    });
  }

  angular.module('core').factory('ClientsAPI', [
    '$resource',
    'API_URL',
    ClientsAPI
  ]);
})();

(function() {
  'use strict';

  function ConfigurationsAPI($resource, API_URL) {
    return $resource(API_URL + '/configurations/:id', {}, {
      update: { method: 'PUT' },
      findOne: {
        method: 'GET',
        url: API_URL + '/configurations/findOne',
        isArray: false,
        responseType: 'json'
      }
    });
  }

  angular.module('core').factory('ConfigurationsAPI', [
    '$resource',
    'API_URL',
    ConfigurationsAPI
  ]);
})();

(function() {
  'use strict';

  function SalesAPI($resource, API_URL) {
    return $resource(API_URL + '/sales/:id', {}, {
      update: { method: 'PUT' },
      count: {
        method: 'GET',
        url: API_URL + '/sales/count',
        isArray: false,
        responseType: 'json'
      }
    });
  }

  angular.module('core').factory('SalesAPI', [
    '$resource',
    'API_URL',
    SalesAPI
  ]);
})();

(function() {
'use strict';

  function HomeConfig($stateProvider) {

    $stateProvider
    .state('start', {
      url: '/inicio',
      parent: 'home',
      templateUrl: 'modules/home/views/home.client.view.html',
      controller: 'HomeCtrl'
    });

  }

  angular.module('home').config([
    '$stateProvider',
    HomeConfig
  ]);
})();

(function() {
  'use strict';

  function HomeCtrl($scope, AppConfig) {
    AppConfig.setTitle();
    var _this = this;

  }

  angular.module('home').controller('HomeCtrl', [
    '$scope',
    'AppConfig',
    HomeCtrl
  ]);
})();

(function() {
'use strict';

  function SalesConfig($stateProvider) {

    $stateProvider
    .state('sales', {
      url: '/ventas',
      parent: 'home',
      templateUrl: 'modules/sales/views/sales.client.view.html',
      controller: 'SalesCtrl'
    })
		.state('saleCreate', {
      url: '/ventas/crear',
      parent: 'home',
      templateUrl: 'modules/sales/views/sales-edition.client.view.html',
      controller: 'SalesEditionCtrl'
    });

  }

  angular.module('sales').config([
    '$stateProvider',
    SalesConfig
  ]);
})();

(function() {
  'use strict';

  function SalesEditionCtrl($scope, $state, $timeout, AppConfig, ConfigurationsAPI, ClientsAPI, ArticlesAPI, SalesAPI, $q) {
    AppConfig.setTitle('Ventas');
    var _this = this;
		// Note: Valores por defecto
    $scope.isContentLoaded = false;
		$scope.currentStep = 1;
		$scope.modal = {};
		$scope.configuration = {
			financingRate: 0,
			hitch: 0,
			configuration: 0
		};
		$scope.totals = {
			hitch: 0,
			bonification: 0,
			totalDebt: 0
		};
		$scope.articles = [];
		$scope.selectedClient = null;
		$scope.selectedArticle = null;
		$scope.abonos = [];

		if (!_this.msgModal) {
			_this.msgModal = angular.element('#msg-modal');
			_this.msgModal.modal();
		}

		$scope.fetchClients = function(query) {
			return ClientsAPI.query({ query: query }).$promise;
		};

		$scope.onClientChange = function(client) {
			if (!client) return;
			$scope.selectedClient = client.originalObject;
		};

		$scope.fetchArticles = function(query) {
			return ArticlesAPI.query({ query: query }).$promise;
		};

		$scope.onArticleChange = function(article) {
			if (!article) return;
			$scope.selectedArticle = article.originalObject;
		};

		$scope.addProductToSale = function() {
			var newArticle = $scope.selectedArticle;
			if (!newArticle) return;

			var foundArticle = _.find($scope.articles, { id: newArticle.id });
			if (foundArticle) {
				$scope.modal.msg = 'Este artículo ya se encuentra en la lista :)';
				_this.msgModal.modal('open');
				return;
			}

			if (newArticle.existence <= 0) {
				console.log('El artículo seleccionado no cuenta con existencia, favor de verificar');
				$scope.modal.msg = 'El artículo seleccionado no cuenta con existencia, favor de verificar';
				_this.msgModal.modal('open');
				return;
			}

			newArticle.quantity = 1;
			newArticle.import = newArticle.price;
			$scope.articles.push(newArticle);
		};

		$scope.removeArticleFromList = function(index) {
			$scope.articles.splice(index, 1);
		};

		this.onArticlesChange = function(articles) {
			var totalImport = 0;

			$scope.articles.forEach(function(article) {
				// Note: Calcular precio para cliente
				article.shownPrice = article.price * (1 + ( $scope.configuration.financingRate * $scope.configuration.maximumPaymentTerms / 100 ) );

				// Note: Validar existencia
				if (!article.quantity || article.quantity <= 0) {
					article.quantity = 1;
				}

				// Note: Validar no exceder existencia
				if (article.quantity && article.quantity > article.existence) {
					console.log('cantidad no puede ser mayor a existencia!')
					$scope.modal.msg = 'cantidad no puede ser mayor a existencia!';
					_this.msgModal.modal('open');
					article.quantity = article.existence;
				}

				// Note: Calcular importe de articulo
				article.import = article.quantity * article.shownPrice;

				// Note: Sumar total a pagar.
				totalImport = totalImport + article.import;
			});

			$scope.totals.hitch = ( $scope.configuration.hitch / 100 ) * totalImport;
			$scope.totals.bonification = $scope.totals.hitch * ( ($scope.configuration.financingRate * $scope.configuration.maximumPaymentTerms) / 100 );
			$scope.totals.totalDebt = totalImport - $scope.totals.hitch - $scope.totals.bonification;
		};

		$scope.next = function() {
			if (!$scope.selectedClient || !$scope.articles.length) {
				console.log('Los datos ingresados no son correctos, favor de verificar');
				$scope.modal.msg = 'Los datos ingresados no son correctos, favor de verificar';
				_this.msgModal.modal('open');
				return;
			}

			// Note: Calcular plazos

			var priceOnePay = $scope.totals.totalDebt / (1 + (($scope.configuration.financingRate * $scope.configuration.maximumPaymentTerms / 100)) );

			for (var i = 1; i <= $scope.configuration.maximumPaymentTerms; i++) {
				var totalAbono = priceOnePay * (1 + ($scope.configuration.financingRate * i) / 100);

				var abono = {
					name: i,
					total: totalAbono,
					quantity: totalAbono / i,
					save: $scope.totals.totalDebt - totalAbono
				};

				$scope.abonos.push(abono);
			}

			$scope.currentStep = 2;
		};

		$scope.selectAbono = function(abono) {
			$scope.selectedAbono = abono;
		}

		this.saveSale = function() {
			if ($scope.savingSale) return;
			$scope.savingSale = true;

			var sale = {
				userId: $scope.selectedClient.id,
				articles: $scope.articles,
				total: $scope.abonos[$scope.selectedAbono].total
			};

			SalesAPI.save(sale).$promise
      .then(function() {
				Materialize.toast('Bien Hecho, Tu venta ha sido registrada correctamente', 5000)
				$state.go('sales');
      })
      .catch(function(err) {
        Materialize.toast('No se guardar la venta, intente nuevamente', 4000);
      })
			.finally(function() {
				$scope.savingSale = false;
			});
		};

		$scope.save = function() {
			if(!$scope.selectedAbono) {
				console.log('Debe seleccionar un plazo para realizar el pago de su compra');
				$scope.modal.msg = 'Debe seleccionar un plazo para realizar el pago de su compra';
				_this.msgModal.modal('open');
				return;
			}
			_this.saveSale();
		};

		$scope.$watch(
			function() {
				return angular.toJson($scope.articles);
			},
			_this.onArticlesChange
		);

    this.fetchConfigurations = function() {
      var configurationsPromise = ConfigurationsAPI.findOne().$promise
      .then(function(configuration) {
        $scope.configuration = configuration;
      })
      .catch(function(err) {
        Materialize.toast('No se pudo obtener configuracion, intente nuevamente', 4000);
      });

      return configurationsPromise;
    };

    this.fetchConfigurationToShowContent = function() {
      $q.all([
        _this.fetchConfigurations()
      ])
      .finally(function() {
        $scope.isContentLoaded = true;
      });
    };

    this.fetchConfigurationToShowContent();

  }

  angular.module('sales').controller('SalesEditionCtrl', [
    '$scope',
		'$state',
		'$timeout',
    'AppConfig',
		'ConfigurationsAPI',
		'ClientsAPI',
		'ArticlesAPI',
    'SalesAPI',
    '$q',
    SalesEditionCtrl
  ]);
})();

(function() {
  'use strict';

  function SalesCtrl($scope, AppConfig, SalesAPI, $q) {
    AppConfig.setTitle('Ventas');
    var _this = this;
    $scope.isContentLoaded = false;
    $scope.sales = [];

		this.fetchSales = function() {
			SalesAPI.query().$promise
			.then(function(sales) {
				$scope.sales = sales;
			})
			.catch(function(err) {
				Materialize.toast('No se pudo obtener ventas, intente nuevamente', 4000);
			})
			.finally(function() {
				$scope.isContentLoaded = true;
			});
		}

    this.fetchSales();
  }

  angular.module('sales').controller('SalesCtrl', [
    '$scope',
    'AppConfig',
    'SalesAPI',
    '$q',
    SalesCtrl
  ]);
})();
