(function() {
  'use strict';

  function SalesCtrl($scope, AppConfig, SalesAPI, $q) {
    AppConfig.setTitle('Ventas');
    var _this = this;
    $scope.isContentLoaded = false;
    $scope.sales = [];

		this.fetchSales = function() {
			SalesAPI.query().$promise
			.then(function(sales) {
				$scope.sales = sales;
			})
			.catch(function(err) {
				Materialize.toast('No se pudo obtener ventas, intente nuevamente', 4000);
			})
			.finally(function() {
				$scope.isContentLoaded = true;
			});
		}

    this.fetchSales();
  }

  angular.module('sales').controller('SalesCtrl', [
    '$scope',
    'AppConfig',
    'SalesAPI',
    '$q',
    SalesCtrl
  ]);
})();
