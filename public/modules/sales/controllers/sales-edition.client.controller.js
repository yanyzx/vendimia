(function() {
  'use strict';

  function SalesEditionCtrl($scope, $state, $timeout, AppConfig, ConfigurationsAPI, ClientsAPI, ArticlesAPI, SalesAPI, $q) {
    AppConfig.setTitle('Ventas');
    var _this = this;
		// Note: Valores por defecto
    $scope.isContentLoaded = false;
		$scope.currentStep = 1;
		$scope.modal = {};
		$scope.configuration = {
			financingRate: 0,
			hitch: 0,
			configuration: 0
		};
		$scope.totals = {
			hitch: 0,
			bonification: 0,
			totalDebt: 0
		};
		$scope.articles = [];
		$scope.selectedClient = null;
		$scope.selectedArticle = null;
		$scope.abonos = [];

		if (!_this.msgModal) {
			_this.msgModal = angular.element('#msg-modal');
			_this.msgModal.modal();
		}

		$scope.fetchClients = function(query) {
			return ClientsAPI.query({ query: query }).$promise;
		};

		$scope.onClientChange = function(client) {
			if (!client) return;
			$scope.selectedClient = client.originalObject;
		};

		$scope.fetchArticles = function(query) {
			return ArticlesAPI.query({ query: query }).$promise;
		};

		$scope.onArticleChange = function(article) {
			if (!article) return;
			$scope.selectedArticle = article.originalObject;
		};

		$scope.addProductToSale = function() {
			var newArticle = $scope.selectedArticle;
			if (!newArticle) return;

			var foundArticle = _.find($scope.articles, { id: newArticle.id });
			if (foundArticle) {
				$scope.modal.msg = 'Este artículo ya se encuentra en la lista :)';
				_this.msgModal.modal('open');
				return;
			}

			if (newArticle.existence <= 0) {
				console.log('El artículo seleccionado no cuenta con existencia, favor de verificar');
				$scope.modal.msg = 'El artículo seleccionado no cuenta con existencia, favor de verificar';
				_this.msgModal.modal('open');
				return;
			}

			newArticle.quantity = 1;
			newArticle.import = newArticle.price;
			$scope.articles.push(newArticle);
		};

		$scope.removeArticleFromList = function(index) {
			$scope.articles.splice(index, 1);
		};

		this.onArticlesChange = function(articles) {
			var totalImport = 0;

			$scope.articles.forEach(function(article) {
				// Note: Calcular precio para cliente
				article.shownPrice = article.price * (1 + ( $scope.configuration.financingRate * $scope.configuration.maximumPaymentTerms / 100 ) );

				// Note: Validar existencia
				if (!article.quantity || article.quantity <= 0) {
					article.quantity = 1;
				}

				// Note: Validar no exceder existencia
				if (article.quantity && article.quantity > article.existence) {
					console.log('cantidad no puede ser mayor a existencia!')
					$scope.modal.msg = 'cantidad no puede ser mayor a existencia!';
					_this.msgModal.modal('open');
					article.quantity = article.existence;
				}

				// Note: Calcular importe de articulo
				article.import = article.quantity * article.shownPrice;

				// Note: Sumar total a pagar.
				totalImport = totalImport + article.import;
			});

			$scope.totals.hitch = ( $scope.configuration.hitch / 100 ) * totalImport;
			$scope.totals.bonification = $scope.totals.hitch * ( ($scope.configuration.financingRate * $scope.configuration.maximumPaymentTerms) / 100 );
			$scope.totals.totalDebt = totalImport - $scope.totals.hitch - $scope.totals.bonification;
		};

		$scope.next = function() {
			if (!$scope.selectedClient || !$scope.articles.length) {
				console.log('Los datos ingresados no son correctos, favor de verificar');
				$scope.modal.msg = 'Los datos ingresados no son correctos, favor de verificar';
				_this.msgModal.modal('open');
				return;
			}

			// Note: Calcular plazos

			var priceOnePay = $scope.totals.totalDebt / (1 + (($scope.configuration.financingRate * $scope.configuration.maximumPaymentTerms / 100)) );

			for (var i = 1; i <= $scope.configuration.maximumPaymentTerms; i++) {
				var totalAbono = priceOnePay * (1 + ($scope.configuration.financingRate * i) / 100);

				var abono = {
					name: i,
					total: totalAbono,
					quantity: totalAbono / i,
					save: $scope.totals.totalDebt - totalAbono
				};

				$scope.abonos.push(abono);
			}

			$scope.currentStep = 2;
		};

		$scope.selectAbono = function(abono) {
			$scope.selectedAbono = abono;
		}

		this.saveSale = function() {
			if ($scope.savingSale) return;
			$scope.savingSale = true;

			var sale = {
				userId: $scope.selectedClient.id,
				articles: $scope.articles,
				total: $scope.abonos[$scope.selectedAbono].total
			};

			SalesAPI.save(sale).$promise
      .then(function() {
				Materialize.toast('Bien Hecho, Tu venta ha sido registrada correctamente', 5000)
				$state.go('sales');
      })
      .catch(function(err) {
        Materialize.toast('No se guardar la venta, intente nuevamente', 4000);
      })
			.finally(function() {
				$scope.savingSale = false;
			});
		};

		$scope.save = function() {
			if(!$scope.selectedAbono) {
				console.log('Debe seleccionar un plazo para realizar el pago de su compra');
				$scope.modal.msg = 'Debe seleccionar un plazo para realizar el pago de su compra';
				_this.msgModal.modal('open');
				return;
			}
			_this.saveSale();
		};

		$scope.$watch(
			function() {
				return angular.toJson($scope.articles);
			},
			_this.onArticlesChange
		);

    this.fetchConfigurations = function() {
      var configurationsPromise = ConfigurationsAPI.findOne().$promise
      .then(function(configuration) {
        $scope.configuration = configuration;
      })
      .catch(function(err) {
        Materialize.toast('No se pudo obtener configuracion, intente nuevamente', 4000);
      });

      return configurationsPromise;
    };

    this.fetchConfigurationToShowContent = function() {
      $q.all([
        _this.fetchConfigurations()
      ])
      .finally(function() {
        $scope.isContentLoaded = true;
      });
    };

    this.fetchConfigurationToShowContent();

  }

  angular.module('sales').controller('SalesEditionCtrl', [
    '$scope',
		'$state',
		'$timeout',
    'AppConfig',
		'ConfigurationsAPI',
		'ClientsAPI',
		'ArticlesAPI',
    'SalesAPI',
    '$q',
    SalesEditionCtrl
  ]);
})();
