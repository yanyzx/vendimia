(function() {
'use strict';

  function SalesConfig($stateProvider) {

    $stateProvider
    .state('sales', {
      url: '/ventas',
      parent: 'home',
      templateUrl: 'modules/sales/views/sales.client.view.html',
      controller: 'SalesCtrl'
    })
		.state('saleCreate', {
      url: '/ventas/crear',
      parent: 'home',
      templateUrl: 'modules/sales/views/sales-edition.client.view.html',
      controller: 'SalesEditionCtrl'
    });

  }

  angular.module('sales').config([
    '$stateProvider',
    SalesConfig
  ]);
})();
