(function() {
  'use strict';

  function ClientsCtrl($scope, AppConfig, ClientsAPI, $q) {
    AppConfig.setTitle('Clientes');
    var _this = this;
		$scope.isContentLoaded = false;
    $scope.clients = [];

		this.fetchClients = function() {
			ClientsAPI.query().$promise
			.then(function(clients) {
				$scope.clients = clients;
			})
			.catch(function(err) {
				Materialize.toast('No se pudo obtener clientes, intente nuevamente', 4000);
			})
			.finally(function() {
				$scope.isContentLoaded = true;
			});
		}

    this.fetchClients();
  }

  angular.module('clients').controller('ClientsCtrl', [
    '$scope',
    'AppConfig',
    'ClientsAPI',
    '$q',
    ClientsCtrl
  ]);
})();
