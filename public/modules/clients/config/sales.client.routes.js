(function() {
'use strict';

  function ClientsConfig($stateProvider) {

    $stateProvider
    .state('clients', {
      url: '/clientes',
      parent: 'home',
      templateUrl: 'modules/clients/views/clients.client.view.html',
      controller: 'ClientsCtrl'
    });

  }

  angular.module('sales').config([
    '$stateProvider',
    ClientsConfig
  ]);
})();
