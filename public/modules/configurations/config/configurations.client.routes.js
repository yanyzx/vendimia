(function() {
'use strict';

  function ConfigurationsConfig($stateProvider) {

    $stateProvider
    .state('configurations', {
      url: '/configuracion',
      parent: 'home',
      templateUrl: 'modules/configurations/views/configurations.client.view.html',
      controller: 'ConfigurationsCtrl'
    });

  }

  angular.module('configurations').config([
    '$stateProvider',
    ConfigurationsConfig
  ]);
})();
