(function() {
  'use strict';

  function ConfigurationsCtrl($scope, AppConfig, ConfigurationsAPI, $q) {
    AppConfig.setTitle('Configuración');
    var _this = this;
		$scope.isContentLoaded = false;
    $scope.configurations = [];

		this.fetchConfigurations = function() {
			ConfigurationsAPI.findOne().$promise
			.then(function(configurations) {
				$scope.configurations[0] = configurations;
			})
			.catch(function(err) {
				Materialize.toast('No se pudo obtener configuraciones, intente nuevamente', 4000);
			})
			.finally(function() {
				$scope.isContentLoaded = true;
			});
		}

    this.fetchConfigurations();
  }

  angular.module('configurations').controller('ConfigurationsCtrl', [
    '$scope',
    'AppConfig',
    'ConfigurationsAPI',
    '$q',
    ConfigurationsCtrl
  ]);
})();
