(function() {
  'use strict';

  function ArticlesCtrl($scope, AppConfig, ArticlesAPI, $q) {
    AppConfig.setTitle('Artículos');
    var _this = this;
		$scope.isContentLoaded = false;
    $scope.articles = [];

		this.fetchArticles = function() {
			ArticlesAPI.query().$promise
			.then(function(articles) {
				$scope.articles = articles;
			})
			.catch(function(err) {
				Materialize.toast('No se pudo obtener articulos, intente nuevamente', 4000);
			})
			.finally(function() {
				$scope.isContentLoaded = true;
			});
		}

    this.fetchArticles();
  }

  angular.module('sales').controller('ArticlesCtrl', [
    '$scope',
    'AppConfig',
    'ArticlesAPI',
    '$q',
    ArticlesCtrl
  ]);
})();
