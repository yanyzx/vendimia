(function() {
'use strict';

  function ArticlesConfig($stateProvider) {

    $stateProvider
    .state('articles', {
      url: '/articulos',
      parent: 'home',
      templateUrl: 'modules/articles/views/articles.client.view.html',
      controller: 'ArticlesCtrl'
    });

  }

  angular.module('articles').config([
    '$stateProvider',
    ArticlesConfig
  ]);
})();
