(function() {
  'use strict';

  function ConfigurationsAPI($resource, API_URL) {
    return $resource(API_URL + '/configurations/:id', {}, {
      update: { method: 'PUT' },
      findOne: {
        method: 'GET',
        url: API_URL + '/configurations/findOne',
        isArray: false,
        responseType: 'json'
      }
    });
  }

  angular.module('core').factory('ConfigurationsAPI', [
    '$resource',
    'API_URL',
    ConfigurationsAPI
  ]);
})();
