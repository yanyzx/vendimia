(function() {
  'use strict';

  function ClientsAPI($resource, API_URL) {
    return $resource(API_URL + '/clients/:id', {}, {
      update: { method: 'PUT' },
      count: {
        method: 'GET',
        url: API_URL + '/clients/count',
        isArray: false,
        responseType: 'json'
      }
    });
  }

  angular.module('core').factory('ClientsAPI', [
    '$resource',
    'API_URL',
    ClientsAPI
  ]);
})();
