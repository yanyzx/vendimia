(function() {
  'use strict';

  function AppConfig($rootScope) {

    return {
      setTitle: function (title, isFull) {
        var DEFAULT_APP_TITLE = 'Vendimia - Venta de artículos en plazos';

        if (title && isFull) {
          $rootScope.title = title;
        }
        else if (title) {
          $rootScope.title = title + ' - Vendimia';
        }
        else {
          $rootScope.title = DEFAULT_APP_TITLE;
        }

      }
    };

  }

  angular.module('core').factory('AppConfig', [
    '$rootScope',
    AppConfig
  ]);
})();
