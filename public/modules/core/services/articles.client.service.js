(function() {
  'use strict';

  function ArticlesAPI($resource, API_URL) {
    return $resource(API_URL + '/articles/:id', {}, {
      update: { method: 'PUT' },
      count: {
        method: 'GET',
        url: API_URL + '/articles/count',
        isArray: false,
        responseType: 'json'
      }
    });
  }

  angular.module('core').factory('ArticlesAPI', [
    '$resource',
    'API_URL',
    ArticlesAPI
  ]);
})();
