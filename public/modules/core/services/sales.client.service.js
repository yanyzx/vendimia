(function() {
  'use strict';

  function SalesAPI($resource, API_URL) {
    return $resource(API_URL + '/sales/:id', {}, {
      update: { method: 'PUT' },
      count: {
        method: 'GET',
        url: API_URL + '/sales/count',
        isArray: false,
        responseType: 'json'
      }
    });
  }

  angular.module('core').factory('SalesAPI', [
    '$resource',
    'API_URL',
    SalesAPI
  ]);
})();
