(function() {
  'use strict';

  function TopbarDirective($rootScope, $timeout, $interval, $state, $stateParams) {
    function link(scope, element, attrs) {

      scope.title = $state.current.appTitle;

      // Hide Header on on scroll down

      var lastScrollTop = 0;
      var move = 0;
      var top = 0;
      var topbarHeight = angular.element('topbar nav').outerHeight() * -1;

      function onScroll(element) {
        var topDistance = $(element).scrollTop();

        var move =  topDistance - lastScrollTop;
        if (topDistance > lastScrollTop) {
          // Scroll Down
          top = top - move;
          if(top < topbarHeight)
            top = topbarHeight;

          angular.element('topbar nav').css('top', top+'px');
        } else {
          // Scroll Up
          top = top - move;
          if(top > 0)
            top = 0;

          angular.element('topbar nav').css('top', top+'px');
        }

        lastScrollTop = topDistance;
      };

      angular.element(window).scroll(function(event){
        onScroll(window);
      });

    }

    return {
      templateUrl: 'modules/core/views/topbar.client.view.html',
      restrict: 'E',
      link: link
    };
  }

  angular.module('core').directive('topbar', [
    '$rootScope',
    '$timeout',
    '$interval',
    '$state',
    '$stateParams',
    TopbarDirective
  ]);
})();
